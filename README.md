#  Grape Disease Detection Tutorial

## 安裝步驟
### 本專案將進行葡萄分級檢測，可參考以下步驟執行：
[開啟ipynb檔執行葡萄檢測](https://drive.google.com/file/d/1qM-l2DreHI8QMEEraQ0kYHX6j2QPVRU_/view?usp=sharing)
葡萄分級檢測將分為兩大步驟：
### 使用Transformer進行 Object Detection
將葡萄影像帶入 Object Detection模型 (DETR)
帶入檢測影像：
<img src="https://i.imgur.com/DPBuS1O.jpg" width="150" height="300" align="middle" />
```
# Upload Grape image.
# img_sample = files.upload()
# img_path = list(img_sample.keys())[0]

# Or Use Sample image
img_path = "test_img/disease_0001.jpg" # disease_0001.jpg, disease_0002.jpg, disease_0003.jpg
```
結果影像
<img src="https://i.imgur.com/Mzitywn.png" width="150" height="300" align="middle" />

並將結果影像切割成多個單顆葡萄影像後，並儲存至雲端暫存區

content
    └─ grape-disease-detection-tutorial
        └─ result 
```
output_dir = "./result/"
crop_one_grape(img, B_Boxes, output_dir)
```

### 使用Transformer進行Anomaly Detection
將單顆葡萄影像帶入 Anomaly Detection 模型 (PaDiM)

<img src="https://i.imgur.com/g3yGKb5.png" width="600" height="250" align="middle" />

輸出多個單顆葡萄影像檢測結果

<img src="https://i.imgur.com/aQGADWX.jpg" width="600" height="250" align="middle" />


